const express = require('express');
const app = express();
app.set('view engine', 'pug');
app.set('views','./views');
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/', (request, response) => {
  response.send('<a href="./todos">Link</a>')
});
var todos = [
  {
    id: 1,
    content: 'Đi chợ',
  },
  {
    id: 2,
    content: 'Nấu cơm',
  },
  {
    id: 3,
    content: 'Rửa bát',
  },
  {
    id: 4,
    content: 'Học code tại CodersX',
  },
];
app.get('/todos', (req, res) => {
  if (req.query.q) {
    let keyword = req.query.q;
    let result = todos.filter((item)=> {
      return item.content.toLowerCase().indexOf(keyword.toLowerCase()) >=0;
    });
    res.render('index',{todoList: result, keyword: keyword});
  }
  else {
    res.render('index',{todoList: todos});
  }
});


app.post('/todos/create', (req,res)=> {
  let data = req.body;
  todos.push(data);
  res.redirect("back");
})
// listen for requests :)
app.listen(3000, () => {
  console.log('Server listening on port ' + process.env.PORT);
});
